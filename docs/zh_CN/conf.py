# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Siril'
copyright = '2024, Free-Astro Team'
author = 'Free-Astro Team'
release = '1.4.0'
import sirilpy # for autodoc
from sirilpy import tksiril # for autodoc
from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.domains import Index
from sphinx import addnodes
from sphinx.builders.html import StandaloneHTMLBuilder

# Set up the module autoreplacement
module_version = sirilpy.__version__
rst_prolog = f"""
.. |module_version| replace:: {module_version}
"""

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx_autodoc_typehints',
    'sphinx.ext.autosectionlabel',
    'sphinx_rtd_theme',
    'sphinx.ext.mathjax',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinxcontrib.cairosvgconverter',
    'sphinx_copybutton',
    'sphinxcontrib.video',
]

autodoc_default_options = {
    'members': True,
    'member-order': 'groupwise',
}

autosectionlabel_prefix_document = True

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'commands/*']

todo_include_todos = True

smartquotes = False

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ['_static']
html_extra_path = ['_static']
html_logo = "_images/org.free_astro.siril.svg"
html_theme_options = {
    "logo_only": True,
}


class CommandDirective(Directive):
    has_content = True
    required_arguments = 1
    optional_arguments = 1
    option_spec = {
        'scriptable': int,  # This defines 'scriptable' as a boolean option
    }

    def run(self):
        # Create the main paragraph node
        para = nodes.paragraph()
        scriptable = self.options.get('scriptable', 1)
        option = 'scriptable' if scriptable else 'nonscriptable'

        target_id = f'{self.arguments[0]}'
        
        # Create the target node
        targetnode = nodes.target()
        targetnode['names'] = [target_id]
        targetnode['ids'] = [target_id]
        self.state.document.note_explicit_target(targetnode)
        
        # Create a section node with a title
        section = nodes.section()
        section['ids'].append(target_id)
        title = nodes.title(text=self.arguments[0])
        section += title

        index_entry = addnodes.index(entries=[('single', self.arguments[0], target_id, 'cmdindex', None)])
        # Store the custom index entry in the environment
        env = self.state.document.settings.env
        if not hasattr(env, 'command_index'):
            env.command_index = []
        env.command_index.append((self.arguments[0], env.docname, target_id))

        # Create the command name with custom class
        command_name = nodes.inline(text=self.arguments[0] + '  ', classes=['commandname'])
        command_name += nodes.substitution_reference(refname=option, reftarget=option)
        command_name += nodes.substitution_reference(refname='indexlink', reftarget='indexlink')
        para += command_name
        
        # Add the description content if present
        if self.content:
            # Add a space between command and description
            para += nodes.Text(' ')
            # Parse the content and add it to the paragraph
            self.state.nested_parse(self.content, self.content_offset, para)
        
        # Return a list containing the paragraph node
        return [index_entry, targetnode, para]

class CustomCommandIndex(Index):
    name = 'cmdindex'
    localname = 'Commands Index'
    shortname = 'Commands'

    def generate(self, docnames=None):
        env = self.domain.env
        content = {}

        # Retrieve the stored command index data
        if not hasattr(env, 'command_index'):
            return [], False

        for entry_name, docname, target_id in env.command_index:
            letter = entry_name[0].upper()
            if letter not in content:
                content[letter] = []
            content[letter].append((entry_name, 0, docname, target_id, '', '', ''))

        sorted_content = sorted(content.items())
        return sorted_content, False

def setup(app):
    app.add_directive('command', CommandDirective)
    app.add_css_file('custom.css')
    app.add_index_to_domain('std', CustomCommandIndex)
    
    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
language = "zh_CN"
locale_dirs = ["../../translated/"]
gettext_compact = "siril-documentation"
��          �      <      �     �     �     �     �     �     �  
   �     �     �  
   �  	               
        )     .     2  �  9     �     �  	   �  	   �  	                  /     ;     T  
   [     f     s     z     �  	   �     �                  	                       
                                                            .fit About Siril Bicubic Bilinear Deconvolution FITS Manual PSF PPMXL PSF Photometry RGB align Redo Stacking Statistics Undo bsc tycho2 Project-Id-Version: Siril
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-07-10 15:37+0000
Last-Translator: Yen-Hsing Lin <julius52700@gmail.com>
Language-Team: Chinese (Traditional) <https://weblate.pixls.us/projects/siril/user-documentation/zh_Hant/>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 5.4.2
 .fit 關於 Siril 雙三次 雙線性 反捲積 FITS 手動PSF (點擴散函數) PPMXL星表 點擴散函數（PSF） 測光 RGB 對齊 取消復原 疊合 統計 復原 bsc星表 tycho2 
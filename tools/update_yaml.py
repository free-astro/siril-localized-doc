import os,sys
from pathdefs import docs, doc
from pathlib import Path
import shutil
import fileinput

ref_yaml = '.readthedocs.yaml'

def update_one_yaml(loc):
    os.chdir(os.path.join(doc))
    src = os.path.join(doc, '..', ref_yaml)
    dst = os.path.join(docs,loc,ref_yaml)

    if Path(src).is_file():
        shutil.copyfile(src, dst)
        with fileinput.input(files = dst, inplace=True) as f:
            for ip_line in f:
                if 'configuration:' in ip_line:
                    ip_line = '   configuration: docs/{:s}/conf.py'.format(loc)
                sys.stdout.write(ip_line)
            end_line = '\n# include the original doc submodule\nsubmodules:\n   include: all\n'
        with open(dst, 'a') as fin:
            fin.write(end_line)
        print('Wrote yaml for {:s}'.format(loc))


def update_all_ymls():
    os.chdir(docs)
    dirs = Path('.').glob('*')
    all_locs=[]

    # updating yaml in ./docs/* subfolders
    for d in dirs:
        loc = str(d)
        all_locs += '{:s} '.format(loc)
        update_one_yaml(str(d))

if __name__ == "__main__":
    update_all_ymls()

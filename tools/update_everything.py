from update_folders import update_all
import update_po
import update_mo


def main():
    update_all()
    update_po.main()
    update_mo.main()


if __name__ == "__main__":
    main()
import os
base = os.path.normpath(os.path.join(os.path.dirname(__file__),'..'))
docs = os.path.normpath(os.path.join(os.path.dirname(__file__),'../docs'))
doc = os.path.normpath(os.path.join(os.path.dirname(__file__),'../siril-doc/doc'))
translated = os.path.normpath(os.path.join(os.path.dirname(__file__),'../translated'))
Siril - localized documentation
===============================

This repository contains localized documentation for Siril. It
is based on master document placed in siril-doc repository.

To translate edit your language file in po directory, changes will be
propagated to generated documents.

Requirements
------------

For manipulating with translations, you need gettext and sphinx installed.
If you want to build the documentation before submitting, you will also need
additional python modules, as listed in requirements.txt. To install them at once:

.. code-block:: sh

    python3 -m pip install -r requirements.txt

Usage (contributors)
--------------------

* Create an account on  `Weblate <https://weblate.org/>`_
* Go to the user documentation component on `this page <https://weblate.pixls.us/projects/siril/>`and translate the language of your choice.

.. note::
    Weblate is the only tool for contributors, we do not accept commits on this repository.

..  warning::

  The syntax must follow strictly ReStructured Text formatting. You can find useful ressources in 
  `siril-doc useful links section <https://gitlab.com/free-astro/siril-doc#useful-links>`_



Usage (maintainers)
-------------------

To start new translation, open a shell in `tools/` and run:

.. code-block:: sh

    python3 tools/create_new.py

This will:

* Create a new language subfolder in `docs/`
* Make symlinks to the files from `siril-doc/docs`
* Create a valid `readthedocs.yaml` and `conf.py` file for that language
* Add the language to the `LANGUAGES` section of the Makefile.
* In the `./po/` folder, copy `siril-documentation.pot` to create a new `.po` file.
* Create a new folder in `./translated/` to a new folder and make the necessary symlinks.

To update the siril-doc submodule:

* Start by pulling the submodule and commit the changes (`git submodule update --remote`)
* Then:

.. code-block:: sh

    python3 tools/update_everything.py

* Commit the changes

To build documentation in given language, say french:

.. code-block:: sh

    cd docs
    sphinx-build fr fr/_build

You can also browse French translated documentation online at `readthedocs. <https://siril.readthedocs.io/fr/latest/>`_

Set-Up (maintainers)
--------------------

Adding a translation on Readthedocs
===================================

* Log in rtd and go to the `dashboard <https://readthedocs.org/dashboard/>`_
* Click ``Import a project`` then ``Import Manually``

Initial set-up
++++++++++++++

* Name: `siril-localized-doc-XX` with `XX` the language code
* Repository: `https://gitlab.com/free-astro/siril-localized-doc.git`
* Default branch: `main`
* Language: Select from drop-down
* Click ``Next`` Then ``Finish``

Finalization
++++++++++++

* Go back to the dashboard and open the new project language
* Go to the ``Admin`` tab and complete with the following:

    + Branch: check `main` is selected
    + Description: siril documentation `language` translation
    + Default version : `stable`
    + Path for .readthedocs.yaml: `docs/XX/.readthedocs.yaml` with `XX` the language code
    + Project homepage: `https://siril.org`

* Go back to the dashboard and open ``Siril`` project
* Go to ``Admin`` tab then ``Translations`` on the left sidebar
* In the drop-down, select the project `siril-localized-doc-XX`

Adding a translation on Weblate
===================================

HOLD

Statistics
----------

.. code-block:: sh

    python3 tools/locale_stats.py

should output something like

.. code-block:: text

    Statistics from de.po
    2813 translated messages, 9 fuzzy translations, 49 untranslated messages.

    Statistics from fr.po
    78 translated messages, 2793 untranslated messages.